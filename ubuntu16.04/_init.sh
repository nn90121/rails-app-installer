#!/bin/bash

echo "========== Aptitude - Install =========="
apt install aptitude -y

sudo add-apt-repository -y -r ppa:chris-lea/node.js
sudo rm -f /etc/apt/sources.list.d/chris-lea-node_js-*.list
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8507EFA5

echo "========== Aptitude - Update =========="
aptitude update
echo "========== Aptitude - Upgrade =========="
aptitude upgrade -y

## 安裝常用工具
echo "========== Install common tools =========="
aptitude install curl git-core python-software-properties software-properties-common bash-completion htop iftop tmux vim wget -y

## 設定時區
echo "========== Setup timezone ...  =========="
dpkg-reconfigure tzdata
