#!/bin/bash

DEPLOY_USER='rails'
NGINX_USER='nginx'
WEB_GROUP='www-data'
RUBY_VERSION='2.1.0'

# 安裝 node.js for assets pipeline
echo "========== [RailsAPP] Installing runtime js library for assets pipelie ... =========="

echo "========== [RailsAPP] Add node.js source for apt ... =========="

if [ -f "/usr/bin/nodejs" ]; then
  echo  "========== node.js Already Installed, if you want to reinstall again, please remove node.js before run this script ... =========="
else
  add-apt-repository ppa:chris-lea/node.js -y

  echo "========== [RailsAPP] Update apt package list and install node.js ... =========="
  aptitude update
  aptitude install nodejs -y
fi

# 新增 rails application deploy user
echo "========== [RailsAPP] Create deploy user = $DEPLOY_USER ... =========="
useradd $DEPLOY_USER -m -G $WEB_GROUP -s /bin/bash
echo "========== [RailsAPP] Setting Deploy user password ... =========="
passwd $DEPLOY_USER

# 授權 public key 登入 deploy user
mkdir /home/$DEPLOY_USER/.ssh/
cp ~/.ssh/authorized_keys /home/$DEPLOY_USER/.ssh/authorized_keys
chown -R $DEPLOY_USER:$DEPLOY_USER ~/.ssh

# 加入 sudoer 要把 deploy user 加到 web group，因為 static file 也需要 nginx 也需要有權限存取
echo "========== [RailsAPP] Add deploy user to sudoer list ... =========="
echo "$DEPLOY_USER  ALL=(ALL:ALL) ALL" >> /etc/sudoers

# 安裝 rvm stable
echo -e "========== [RailsAPP] Installing RVM ... =========="
su -l -c "\curl -sSL https://get.rvm.io | bash -s stable" $DEPLOY_USER

# 更新 RVM
echo "========== [RailsAPP] Update RVM ... =========="
su -l -c "rvm get head" $DEPLOY_USER