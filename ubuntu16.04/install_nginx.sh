#!/bin/bash

# add newest nginx repo, update, and install
echo "========== [Nginx] Add newest nginx repo, update, install ... =========="
add-apt-repository ppa:nginx/stable
aptitude update
aptitude install nginx -y

# restart nginx (restart nginx)
echo "========== [Nginx] Restart ... =========="
/etc/init.d/nginx restart
