#!/bin/bash

DEPLOY_USER='rails'
NGINX_USER='nginx'
WEB_GROUP='www-data'
RUBY_VERSION='2.1.0'

# 安裝 Ruby + Bundler
echo "========== [RailsAPP] Installing Ruby $RUBY_VERSION ... =========="
su -l -c "rvm install $RUBY_VERSION" $DEPLOY_USER
su -l -c "rvm --default use $RUBY_VERSION" $DEPLOY_USER

echo "========== [RailsAPP] Installing Bundler ... =========="
su -l -c "gem install bundler" $DEPLOY_USER

# 安裝 mysql lib client for gem => mysql2
sudo apt-get install libmysqlclient-dev -y