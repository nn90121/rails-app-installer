#!/bin/bash

echo "========== Install Percona ...  =========="
# Add apt source
IF_SOURCE_ADDED=$(cat /etc/apt/sources.list | grep repo.percona.com | wc -l)
if [ $IF_SOURCE_ADDED == 0 ]; then
  # Add percona mysql apt key
  echo  "========== [Percona] Add percona mysql apt key ... =========="
  apt-key adv --keyserver keys.gnupg.net --recv-keys 1C4CBDCDCD2EFD2A

  UBUNTU_DISTRIB=$(cat /etc/*-release | grep DISTRIB_CODENAME | awk 'BEGIN {FS="="}{print $2}')
  echo  "========== [Percona] Add percona mysql apt source.list ... =========="
  echo  "========== [Percona] DISTRIB_CODENAME=$UBUNTU_DISTRIB ... =========="
  echo "deb http://repo.percona.com/apt $UBUNTU_DISTRIB main" >> /etc/apt/sources.list
  echo "deb-src http://repo.percona.com/apt $UBUNTU_DISTRIB main" >> /etc/apt/sources.list

  echo  "========== [Percona] Update apt list ... =========="
  aptitude update
else
  echo  "========== [Percona] source exists ... =========="
fi


if [ -f "/usr/bin/mysqld_safe" ]; then
  echo  "========== MySQL Already Installed, if you want to reinstall again, please remove mysql before run this script ... =========="
else
  # Setting mysql configuration
  echo  "========== [Percona] Install mysql ... =========="
  aptitude install percona-server-server-5.5 percona-server-client-5.5 -y
  /etc/init.d/mysql stop

  # Restart percona server
  /etc/init.d/mysql restart
fi
